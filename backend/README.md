# Cyberjudo Backend

## Dependencies
- a [postgresql database](https://www.postgresql.org/docs/current/)
- a directory with image files corresponding to the names in the database

To initialize the database, execute the `schema-postgres.sql` file in the database.
That will drop and (re)create all tables required by this backend.
The statements are wrapped in a transaction, so it will rollback automatically if something fails.
That file is good reading material to understand the data model underlying this backend.

## Development Setup

```shell script
sudo apt-get install python3-pip
pip3 install --user pipenv
# cd into this directory
pipenv --python 3.7
pipenv install

# create a configuration file for the development instance
cp config-example.yml config-dev.yml
$EDITOR config-dev.yml

# run development server:
CJ_BACKEND_SETTINGS=$(pwd)/config-dev.yml pipenv run python launch.py
```

## Production Setup

```shell script
# build the backend docker image
docker build -t cyberjudo-backend .

# create a configuration file
cp config-example.yml path/to/backend-config/config.yml
$EDITOR path/to/backend-config/config.yml

# run a backend docker container
docker run -it --rm -v "path/to/backend-config:/etc/cyberjudo:ro" \
 -v "path/to/static-images:path/to/static-images/as/in/config.yml:ro" \
 -p 5000:80 cyberjudo-backend
```

## Usage
The backend exposes a REST API on port 5000 (in development) or 80 (in production).
The available resources are documented at the `/docs` and `/redoc` URLs.

### Admin Access
Modifying database content requires admin access. You can create an admin user using this statement:
```sql
INSERT INTO admins (name, password_hash) VALUES ('<admin>', crypt('<password>', gen_salt('md5')));
```
pro-tip: Use a password with [enough entropy](https://xkcd.com/936).
