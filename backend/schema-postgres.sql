begin;
drop table if exists techniques_aliases;
drop table if exists images;
drop table if exists techniques;
drop table if exists "groups";
drop table if exists subcategories;
drop table if exists categories;
drop table if exists belts;
drop table if exists ranks;
drop table if exists admin_tokens;


create table ranks
(
    position   SMALLINT PRIMARY KEY,
    japan_name varchar(25) NOT NULL,
    default_belt_color varchar(30) -- must be nullable to resolve circular dependency with belts
);

create table belts
(
    position SMALLINT NOT NULL,
    color varchar(30) NOT NULL,
    german_color_name varchar(30) NOT NULL,
    UNIQUE (position, color),
    FOREIGN KEY (position) REFERENCES ranks (position)
);

ALTER TABLE ranks ADD FOREIGN KEY (position, default_belt_color) REFERENCES belts (position, color);

create table categories
(
    id          SMALLSERIAL PRIMARY KEY,
    german_name varchar(25) NOT NULL UNIQUE,
    japan_name  varchar(25) NOT NULL UNIQUE
);

create table subcategories
(
    id          SMALLSERIAL PRIMARY KEY,
    german_name varchar(25) NOT NULL,
    japan_name  varchar(25) NOT NULL,
    category_id SMALLINT    NOT NULL,
    FOREIGN KEY (category_id) REFERENCES categories (id),
    UNIQUE (german_name, category_id),
    UNIQUE (japan_name, category_id)
);

create table "groups"
(
    id             SMALLSERIAL PRIMARY KEY,
    german_name    varchar(25) NOT NULL,
    japan_name     varchar(25) NOT NULL,
    subcategory_id SMALLINT    NOT NULL,
    UNIQUE (german_name, subcategory_id),
    UNIQUE (japan_name, subcategory_id),
    FOREIGN KEY (subcategory_id) REFERENCES subcategories (id)
);

create table techniques
(
    id                SMALLSERIAL PRIMARY KEY,
    japan_name        varchar(25) NOT NULL UNIQUE,
    group_id          SMALLINT    NOT NULL,
    description       varchar(100),
    description_left  varchar(100),
    description_right varchar(100),
    rank_position     SMALLINT,
    variant_of        SMALLINT,
    CHECK ( id <> variant_of ),
    FOREIGN KEY (group_id) REFERENCES groups (id),
    FOREIGN KEY (rank_position) REFERENCES ranks (position),
    FOREIGN KEY (variant_of) REFERENCES techniques (id)
);

create table images
(
    technique_id SMALLINT    NOT NULL,
    name         varchar(100) NOT NULL,
    rank_position SMALLINT,
    PRIMARY KEY (name, technique_id),
    FOREIGN KEY (technique_id) REFERENCES techniques (id),
    FOREIGN KEY (rank_position) REFERENCES ranks (position)
);

comment on column images.rank_position is 'from this rank on, a judoka should detect which technique the image shows';

create table techniques_aliases
(
    technique_id SMALLINT,
    alias        varchar(25) NOT NULL,
    PRIMARY KEY (alias, technique_id),
    FOREIGN KEY (technique_id) REFERENCES techniques (id)
);

create extension if not exists pgcrypto;

create table admins
(
    name text primary key,
    password_hash text not null,
    changed_at timestamptz not null default now()
);

commit;