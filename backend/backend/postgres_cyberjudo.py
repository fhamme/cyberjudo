from random import shuffle

from psycopg_pool import ConnectionPool
from psycopg.rows import dict_row

from .cyberjudo import *


def quote(value):
    if isinstance(value, int):
        return str(value)
    value = str(value).replace('\\', '\\\\').replace("'", r"\'")
    return f"'{value}'"


class PostgresCyberJudo(CyberJudo):
    def __init__(self, host: str, database: str, user: str, password: str, port: int):
        connection_info = dict(host=host, port=port, user=user, password=password, dbname=database)
        connection_str = ' '.join([f'{key}={quote(value)}' for key, value in connection_info.items()])
        self.pool = ConnectionPool(connection_str, min_size=1, max_size=10, open=True)

    def query(self, statement: str, args: dict = None, row_class=None, return_rows=True):
        with self.pool.connection() as connection:
            with connection.cursor(row_factory=dict_row) as cursor:
                cursor.execute(statement, args)
                if not return_rows:
                    return
                if row_class is None:
                    return cursor.fetchall()
                else:
                    return [row_class(**row) for row in cursor.fetchall()]

    def validate_credentials(self, username: str, password: str) -> bool:
        result = self.query("select password_hash = crypt(%(password)s, password_hash) as is_valid "
                            " from admins where name = %(name)s;",
                            args={'name': username, 'password': password})
        return len(result) != 0 and result[0]['is_valid']

    def ranks(self) -> List[Rank]:
        return [
            Rank(position=row[0], japan_name=row[1], default_belt_color=row[2])
            for row in self.query("select position, japan_name, default_belt_color from ranks order by position;")
        ]

    def belts(self, rank_position: int = None) -> List[Belt]:
        if rank_position is None:
            statement_template = "select position, color, german_color_name from belts order by position;"
        else:
            statement_template = "select position, color, german_color_name from belts where position = %(rank_position)s;"
        return [
            Belt(position=row[0], color=row[1], german_color_name=row[2])
            for row in self.query(statement_template, args={'rank_position': rank_position})
        ]

    def categories(self) -> List[Category]:
        return [
            Category(id=row[0], german_name=row[1], japan_name=row[2])
            for row in self.query("select id, german_name, japan_name from categories order by id;")
        ]

    def subcategories(self, category_ids: List[int] = None) -> List[Subcategory]:
        if category_ids is None:
            category_ids = list()
        if len(category_ids) == 0:
            return [
                Subcategory(id=row[0], german_name=row[1], japan_name=row[2], category_id=row[3])
                for row in self.query("select id, german_name, japan_name, category_id from subcategories order by id;")
            ]
        return [
            Subcategory(id=row[0], german_name=row[1], japan_name=row[2], category_id=row[3])
            for row in self.query(
                "select id, german_name, japan_name, category_id from subcategories"
                " where category_id = any(%(category_ids)s) order by id;",
                args={'category_ids': category_ids})
        ]

    def techniques(self, category_ids: Set[int] = None, subcategory_ids: Set[int] = None,
                   group_ids: Set[int] = None, rank_positions: Set[int] = None) -> List[Technique]:
        if category_ids is None:
            category_ids = set()
        if subcategory_ids is None:
            subcategory_ids = set()
        if group_ids is None:
            group_ids = set()

        # union with all subcategories from categories
        if len(category_ids) > 0:
            subcategory_ids |= {row[0] for row in
                                self.query("select id from subcategories where category_id = any(%(category_ids)s)",
                                           args={'category_ids': list(category_ids)})}
        # union with all groups from subcategories
        if len(subcategory_ids) > 0:
            group_ids |= {row[0] for row in
                          self.query("select id from groups where subcategory_id = any(%(subcategory_ids)s)",
                                     args={'subcategory_ids': list(subcategory_ids)})}

        # in case the categories or subcategories did not contain any groups, no techniques match the filter
        if len(group_ids) == 0 and (len(category_ids) != 0 or len(subcategory_ids) != 0):
            return []

        # convert to list for psycopg2
        rank_positions = list() if rank_positions is None else list(rank_positions)
        group_id_list = list(group_ids)
        return [
            Technique(
                id=row[0],
                japan_name=row[1],
                group_id=row[2],
                description=row[3],
                description_left=row[4],
                description_right=row[5],
                rank_position=row[6],
                variant_of=row[7]
            ) for row in self.query("""
                select id, japan_name, group_id, description, description_left, description_right, rank_position, variant_of
                from techniques
                where (cardinality(%(rank_positions)s::smallint[]) = 0 or rank_position = any(%(rank_positions)s)) 
                and (cardinality(%(group_ids)s::smallint[]) = 0 or group_id = any(%(group_ids)s))
                order by rank_position, id;
                """, args={
                'group_ids': group_id_list,
                'rank_positions': rank_positions,
            })
        ]

    def groups(self, category_ids: Set[int] = None, subcategory_ids: Set[int] = None):
        # convert to list for psycopg2
        category_ids = list() if category_ids is None else list(category_ids)
        if subcategory_ids is None:
            subcategory_ids = set()
        if len(category_ids) > 0:
            subcategory_ids |= {row[0] for row in self.query(
                "select distinct id from subcategories where category_id = any(%(category_ids)s);",
                args={'category_ids': category_ids})}
        if len(subcategory_ids) == 0:
            return [
                Group(id=row[0], german_name=row[1], japan_name=row[2], subcategory_id=row[3])
                for row in self.query("select id, german_name, japan_name, subcategory_id from groups order by id;")
            ]

        # convert to list for psycopg2
        subcategory_ids = list(subcategory_ids)
        return [
            Group(id=row[0], german_name=row[1], japan_name=row[2], subcategory_id=row[3])
            for row in self.query("select id, german_name, japan_name, subcategory_id from groups"
                                  " where subcategory_id = any(%(subcategory_ids)s) order by id;",
                                  args={'subcategory_ids': subcategory_ids})
        ]

    def get_technique(self, technique_id: int) -> Technique:
        rows = self.query("select id, japan_name, group_id, description, description_left, description_right,"
                          " rank_position, variant_of from techniques where id = %(id)s;", args={'id': technique_id})
        if len(rows) == 0:
            raise Exception(f'no technique found with id {technique_id}')
        return Technique(**rows[0])

    def image_is_known(self, image_path: str) -> bool:
        rows = self.query("select count(*) from images where name = %(image_path)s", args={'image_path': image_path})
        return rows[0]['count'] > 0

    def memory_pieces(self, max_rank: int, max_count: int, cjc: CyberJudoCrypto, name_type: MemoryNameType,
                      category_id: int = None) -> MemoryPieces:
        """
        This method provides technique and image names for a memory game.
        It returns up to max_count+1 random japan technique names along with one corresponding random image each,
        except for one random image, which is left out to enforce a player decision even at the last name-image-match.
        """

        # there are more performant ways than `order by random()`,
        # but this should not matter for the expected table size
        # a technique is not used if it has no images (because of inner join)
        if name_type == MemoryNameType.TECHNIQUE:
            rows = self.query("""
                select t.id as name_id, t.japan_name as name, (array_agg(i.name order by random()))[1] as random_image
                from techniques t
                join images i on (t.id = i.technique_id)
                join groups g on (t.group_id = g.id)
                join subcategories s on (g.subcategory_id = s.id)
                where t.rank_position <= %(max_rank)s and (i.rank_position is null or i.rank_position <= %(max_rank)s)
                    and (%(category_id)s::int is null or s.category_id = %(category_id)s::int)    
                group by t.id, t.japan_name
                order by random() limit %(max_count)s;""",
                              args={'max_rank': max_rank, 'max_count': max_count + 1, 'category_id': category_id})
        else:
            rows = self.query("""
                select g.id as name_id, g.japan_name as name, (array_agg(i.name order by random()))[1] as random_image
                from techniques t
                join images i on (t.id = i.technique_id)
                join groups g on (t.group_id = g.id)
                join subcategories s on (g.subcategory_id = s.id)
                where t.rank_position <= %(max_rank)s and (i.rank_position is null or i.rank_position <= %(max_rank)s)
                    and (%(category_id)s::int is null or s.category_id = %(category_id)s::int)
                group by g.japan_name, g.id
                order by random() limit %(max_count)s;""",
                              {'max_rank': max_rank, 'category_id': category_id, 'max_count': max_count + 1})

        memory_techniques = [MemoryName(id=row['name_id'], name=row['name']) for row in rows]
        # shuffle again to get different orderings of names and image_ids
        shuffle(rows)
        # take one image_id less to get more names than image_ids
        memory_image_ids = [cjc.encrypt(row['random_image']) for row in rows[:-1]]
        return MemoryPieces(names=memory_techniques, image_ids=memory_image_ids, name_type=name_type)

    def validate_submissions(self, submissions: List[DecryptedMemorySubmission]) -> List[MemorySubmissionResult]:
        """
        This method reveals the correct answers to the name-image-matches of a memory game.
        """
        submission_results = list()
        for submission in submissions:
            if submission.name_type == MemoryNameType.TECHNIQUE:
                selected_technique = self.get_technique(submission.name_id)
                selected_name = MemoryName(id=selected_technique.id, name=selected_technique.japan_name)
                image_technique_ids = [row[0] for row in
                                       self.query("select technique_id from images where name = %(name)s;",
                                                  args={'name': submission.image_name})]
                if selected_name.id in image_technique_ids:
                    is_correct = True
                    name_of_selected_image = selected_name
                else:
                    if len(image_technique_ids) > 1:
                        raise Exception(
                            f'ambiguous image name: image_name={submission.image_name} technique_ids={image_technique_ids}')
                    is_correct = False
                    technique_of_selected_image = self.get_technique(image_technique_ids[0])
                    name_of_selected_image = MemoryName(id=technique_of_selected_image.id,
                                                        name=technique_of_selected_image.japan_name)

                # if there are multiple images for a technique, choose one at random
                image_names = self.query(
                    "select name from images where technique_id=%(technique_id)s order by random() limit 1;",
                    args={'technique_id': selected_name.id})
                if len(image_names) == 0:
                    image_of_selected_name = None
                else:
                    image_of_selected_name = image_names[0][0]
                self.update_technique_statistics(selected_technique.id, is_correct)
            else:
                selected_groups = self.query("""
                    select g.id as group_id, g.japan_name as group_name, array_agg(i.name order by random()) as image_names
                    from groups g
                    left join techniques t on g.id = t.group_id
                    left join images i on t.id = i.technique_id
                    where g.id = %(id)s
                    group by g.id, g.japan_name;""",
                                             {'id': submission.name_id})
                if len(selected_groups) != 1:
                    raise Exception(f'no group found with id {submission.name_id}')
                selected_group = selected_groups[0]
                if len(selected_group['image_names']) == 0:
                    raise Exception(f'no image found for group with id {submission.name_id}')
                groups_of_image = self.query("""
                    select g.id, g.japan_name as name from images i
                    join techniques t on i.technique_id = t.id
                    join groups g on t.group_id = g.id
                    where i.name = %(image_name)s
                    group by g.id;
                    """, {'image_name': submission.image_name})
                if len(groups_of_image) != 1:
                    raise Exception(
                        f'found {len(groups_of_image)} groups for image {submission.image_name}, expected 1')
                is_correct = submission.image_name in selected_group['image_names']
                selected_name = MemoryName(id=selected_group['group_id'], name=selected_group['group_name'])
                name_of_selected_image = MemoryName(**groups_of_image[0])
                image_of_selected_name = selected_group['image_names'][0]
                self.update_group_statistics(selected_name.id, is_correct)

            submission_results.append(MemorySubmissionResult(
                is_correct=is_correct,
                selected_name=selected_name,
                name_of_selected_image=name_of_selected_image,
                selected_image=submission.image_name,
                image_of_selected_name=image_of_selected_name
            ))
        if len(submissions) > 0:
            # the name type should be the same for all in 'submissions'
            if submissions[0].name_type == MemoryNameType.TECHNIQUE:
                self.count_technique_game_submission()
            else:
                self.count_group_game_submission()
        return submission_results

    def add_rank(self, rank: Rank):
        self.query("""
            insert into ranks (japan_name, position, default_belt_color)
            values (%(japan_name)s, %(position)s, %(default_belt_color)s);
            """, rank.model_dump(mode='json'), return_rows=False)

    def add_belt(self, belt: Belt):
        self.query("""
            insert into belts (position, color, german_color_name)
            values (%(position)s, %(color)s, %(german_color_name)s)""",
                   args=belt.model_dump(mode='json'), return_rows=False)

    def add_category(self, category: Category):
        self.query("""
            insert into categories (german_name, japan_name)
            values (%(german_name)s, %(japan_name)s)""",
                   category.model_dump(mode='json'), return_rows=False)

    def add_subcategory(self, subcategory: Subcategory):
        self.query(
            "insert into subcategories (german_name, japan_name, category_id)"
            " values (%(german_name)s, %(japan_name)s, %(category_id)s)",
            subcategory.model_dump(mode='json'), return_rows=False
        )

    def add_group(self, group: Group):
        self.query(
            "insert into groups (german_name, japan_name, subcategory_id)"
            " values (%(german_name)s, %(japan_name)s, %(subcategory_id)s)",
            group.model_dump(mode='json'), return_rows=False
        )

    def add_technique(self, technique: Technique):
        self.query(
            """
            insert into techniques (
                japan_name,
                group_id,
                description,
                description_left,
                description_right,
                rank_position,
                variant_of
            ) values (
                %(japan_name)s,
                %(group_id)s,
                %(description)s,
                %(description_left)s,
                %(description_right)s,
                %(rank_position)s,
                %(variant_of)s
            );
            """,
            args=technique.model_dump(mode='json'), return_rows=False
        )

    def update_technique_statistics(self, technique_id: int, is_correct: bool):
        if is_correct:
            self.query("""
                insert into technique_game_statistics (technique_id, correct_answers)
                values (%(technique_id)s, 1)
                on conflict (technique_id) do update
                set correct_answers = technique_game_statistics.correct_answers + 1;""",
                       {'technique_id': technique_id}, return_rows=False)
        else:
            self.query("""
                insert into technique_game_statistics (technique_id, wrong_answers)
                values (%(technique_id)s, 1)
                on conflict (technique_id) do update
                set wrong_answers = technique_game_statistics.wrong_answers + 1;""",
                       {'technique_id': technique_id}, return_rows=False)

    def update_group_statistics(self, group_id: int, is_correct: bool):
        if is_correct:
            self.query("""
                insert into group_game_statistics (group_id, correct_answers)
                values (%(group_id)s, 1)
                on conflict (group_id) do update set correct_answers = group_game_statistics.correct_answers + 1;""",
                       {'group_id': group_id}, return_rows=False)
        else:
            self.query("""
                insert into group_game_statistics (group_id, wrong_answers)
                values (%(group_id)s, 1)
                on conflict (group_id) do update set wrong_answers = group_game_statistics.wrong_answers + 1;""",
                       {'group_id': group_id}, return_rows=False)

    def count_technique_game_submission(self):
        self.query("""select nextval('submitted_technique_games');""", return_rows=False)

    def count_group_game_submission(self):
        self.query("""select nextval('submitted_group_games');""", return_rows=False)

    def count_game_start(self, name_type: MemoryNameType):
        if name_type == MemoryNameType.TECHNIQUE:
            self.query("""select nextval('started_technique_games');""", return_rows=False)
        else:
            self.query("""select nextval('started_group_games');""", return_rows=False)

    def get_global_counters(self) -> Mapping[str, int]:
        counters = dict()
        for row in self.query("""select counter_name, last_value from counters;"""):
            counters[row['counter_name']] = row['last_value']
        return counters

    def get_technique_game_statistics(self) -> List[TechniqueGameStatistic]:
        return self.query("""
            select s.technique_id, s.correct_answers, s.wrong_answers, t.japan_name
            from technique_game_statistics s join techniques t on s.technique_id = t.id
            order by wrong_answers desc, correct_answers, japan_name;""", row_class=TechniqueGameStatistic)

    def get_group_game_statistics(self) -> List[GroupGameStatistic]:
        return self.query("""
        select s.group_id, s.correct_answers, s.wrong_answers, g.japan_name
        from group_game_statistics s join groups g on s.group_id = g.id
        order by wrong_answers desc, correct_answers, japan_name;""", row_class=GroupGameStatistic)
