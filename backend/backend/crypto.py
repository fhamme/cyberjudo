import base64

from cryptography.fernet import Fernet


class CyberJudoCrypto:
    def __init__(self):
        self._fernet = Fernet(Fernet.generate_key())

    def encrypt(self, string: str):
        return base64.urlsafe_b64encode(self._fernet.encrypt(string.encode(encoding='UTF-8'))).decode('ascii')

    def decrypt(self, string: str):
        return self._fernet.decrypt(base64.urlsafe_b64decode(string)).decode('UTF-8')
