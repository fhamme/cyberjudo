import os
import sys

import yaml
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .crypto import CyberJudoCrypto
from .postgres_cyberjudo import PostgresCyberJudo
from .v1_images import get_images_router as v1_images_router
from .v1_memory_game import get_memory_game_router as v1_memory_game_router
from .v1_modifications import get_modifications_router as v1_modifications_router
from .v1_readonly import get_readonly_router as v1_readonly_router
from .v1_statistics import get_statistics_router as v1_statistics_router


def get_config() -> dict:
    env_var_name = 'CJ_BACKEND_CONFIG'
    try:
        config_file_path = os.environ[env_var_name]
    except KeyError:
        print(f'environment variable {env_var_name} must be set to a configuration file path!', file=sys.stderr)
        sys.exit(1)
    with open(config_file_path) as file:
        return yaml.safe_load(file.read())


config = get_config()

app = FastAPI(version='2.2', title='Cyber-Judo API', description='learn judo online', root_path=config.get('root-path'))

app.add_middleware(CORSMiddleware, allow_origins=config['cors']['allowed_origins'], allow_credentials=True,
                   allow_methods=["*"], allow_headers=["*"])

cyber_judo = PostgresCyberJudo(**config['db'])
cjc = CyberJudoCrypto()
app.include_router(v1_readonly_router(cyber_judo), tags=['readonly'])
app.include_router(v1_modifications_router(cyber_judo), tags=['techniques modifications'])
app.include_router(v1_memory_game_router(cyber_judo, cjc), tags=['memory game'], prefix='/memory')
app.include_router(v1_images_router(cyber_judo, config['static-images']['dir'], cjc), tags=['images'], prefix='/images')
app.include_router(v1_statistics_router(cyber_judo), tags=['statistics'], prefix='/statistics')
