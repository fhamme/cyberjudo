from typing import Set, Mapping

from .crypto import CyberJudoCrypto
from .models import *


class CyberJudo:
    def validate_credentials(self, username: str, password: str) -> bool:
        raise NotImplemented

    def add_rank(self, rank: Rank):
        raise NotImplemented

    def add_belt(self, belt: Belt):
        raise NotImplemented

    def add_category(self, category: Category):
        raise NotImplemented

    def add_subcategory(self, subcategory: Subcategory):
        raise NotImplemented

    def add_group(self, group: Group):
        raise NotImplemented

    def add_technique(self, technique: Technique):
        raise NotImplemented

    def ranks(self) -> List[Rank]:
        raise NotImplemented

    def belts(self, rank_position: int = None) -> List[Belt]:
        raise NotImplemented

    def categories(self) -> List[Category]:
        raise NotImplemented

    def subcategories(self, category_ids: List[int] = None) -> List[Subcategory]:
        raise NotImplemented

    def groups(self, category_ids: Set[int] = None, subcategory_ids: Set[int] = None) -> List[Group]:
        raise NotImplemented

    def techniques(self, category_ids: Set[int] = None, subcategory_ids: Set[int] = None,
                   group_ids: Set[int] = None, rank_positions: Set[int] = None) -> List[Technique]:
        raise NotImplemented

    def image_is_known(self, image_path: str) -> bool:
        raise NotImplemented

    def memory_pieces(self, max_rank: int, max_count: int, cjc: CyberJudoCrypto, name_type: MemoryNameType,
                      category_id: int = None) -> MemoryPieces:
        raise NotImplemented

    def validate_submissions(self, submissions: List[DecryptedMemorySubmission]) -> List[MemorySubmissionResult]:
        raise NotImplemented

    def count_game_start(self, name_type: MemoryNameType):
        raise NotImplemented

    def get_global_counters(self) -> Mapping[str, int]:
        raise NotImplemented

    def get_technique_game_statistics(self) -> List[TechniqueGameStatistic]:
        raise NotImplemented

    def get_group_game_statistics(self) -> List[GroupGameStatistic]:
        raise NotImplemented
