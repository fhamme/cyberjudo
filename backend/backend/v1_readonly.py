from typing import Union, Set

from fastapi import APIRouter, Query

from .cyberjudo import CyberJudo
from .models import *


def get_readonly_router(cyberjudo: CyberJudo) -> APIRouter:
    router = APIRouter()

    @router.get('/ranks', response_model=Union[List[Rank], List[RankWithBelts]])
    def get_ranks(with_belts: bool = False):
        ranks = cyberjudo.ranks()
        if with_belts:
            belts = frozenset(cyberjudo.belts())
            return [RankWithBelts(
                rank=rank, belts=list({belt for belt in belts if belt.position == rank.position})
            ) for rank in ranks]
        else:
            return ranks

    @router.get('/belts', response_model=List[Belt])
    def get_belts():
        return cyberjudo.belts()

    @router.get('/ranks/{rank_position}/belts')
    def get_belts(rank_position: int):
        return cyberjudo.belts(rank_position)

    @router.get('/categories', response_model=List[Category])
    def get_categories():
        return cyberjudo.categories()

    @router.get('/subcategories', response_model=List[Subcategory])
    def get_subcategories(category_ids: List[int] = Query(None)):
        return cyberjudo.subcategories(category_ids=category_ids)

    @router.get('/groups', response_model=List[Group],
                description='if set, the query parameters will be joined using logical OR to form the filter condition')
    def get_groups(category_ids: Set[int] = Query(None), subcategory_ids: Set[int] = Query(None)):
        return cyberjudo.groups(category_ids, subcategory_ids)

    @router.get('/techniques', response_model=List[Technique],
                description='category_ids, subcategory_ids and group_ids are ORed together,'
                            ' and ANDed with rank_positions.'
                            ' If none of a type is specified, it matches all.')
    def get_techniques(category_ids: Set[int] = Query(None), subcategory_ids: Set[int] = Query(None),
                       group_ids: Set[int] = Query(None), rank_positions: Set[int] = Query(None)):
        return cyberjudo.techniques(category_ids, subcategory_ids, group_ids, rank_positions)

    return router
