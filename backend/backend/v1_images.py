from os import path

from fastapi import APIRouter, HTTPException
from fastapi.responses import FileResponse

from .crypto import CyberJudoCrypto
from .cyberjudo import CyberJudo


def get_images_router(cyberjudo: CyberJudo, images_dir: str, cjc: CyberJudoCrypto):
    router = APIRouter()

    @router.get('/named-images/{name:path}', response_class=FileResponse,
                description='this directly returns an image (mime type depending on the file), if known')
    def get_image_by_name(name: str):
        full_path = path.realpath(path.join(images_dir, name))
        if path.commonpath([images_dir, full_path]) != images_dir:
            # prevent accessing files outside images_dir with "../"
            raise HTTPException(status_code=422, detail='invalid image path')

        if not cyberjudo.image_is_known(name):
            raise HTTPException(status_code=404, detail=f'image unknown')

        if not path.isfile(full_path):
            raise HTTPException(status_code=500, detail=f'known image not found')

        return FileResponse(full_path)

    @router.get('/anonymous-images/{image_id}', response_class=FileResponse)
    def get_image_by_anonymous_id(image_id: str):
        """
        This directly returns an image (Content-Type type depending on the file), if known.
        In contrast to the /named-images/{name} endpoint, this takes encrypted image names using
        the {image_id} argument.
        An encrypted image name can be obtained from a memory game endpoint.
        """
        real_image_name = cjc.decrypt(image_id)
        return get_image_by_name(real_image_name)

    return router
