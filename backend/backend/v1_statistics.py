from typing import List, Mapping

from fastapi import APIRouter, Depends

from .auth import is_admin
from .cyberjudo import CyberJudo
from .models import TechniqueGameStatistic, GroupGameStatistic


def get_statistics_router(cyberjudo: CyberJudo) -> APIRouter:
    router = APIRouter(dependencies=[Depends(is_admin(cyberjudo))])

    @router.get('/global-counters', response_model=Mapping[str, int])
    def _get_global_counters():
        """
        available counters:
        - started_technique_games
        - started_group_games
        - submitted_group_games
        - submitted_technique_games
        """
        return cyberjudo.get_global_counters()

    @router.get('/techniques', response_model=List[TechniqueGameStatistic])
    def _get_technique_game_statistics():
        return cyberjudo.get_technique_game_statistics()

    @router.get('/groups', response_model=List[GroupGameStatistic])
    def _get_group_game_statistics():
        return cyberjudo.get_group_game_statistics()

    return router
