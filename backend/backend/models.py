from enum import Enum
from typing import List, Optional

from pydantic import BaseModel, Field


class HashableBaseModel(BaseModel):
    def __hash__(self):
        return hash((type(self),) + tuple(self.__dict__.values()))


class Rank(BaseModel):
    japan_name: str
    position: int
    default_belt_color: Optional[str] = None


class Belt(HashableBaseModel):
    position: int
    color: str
    german_color_name: str


class RankWithBelts(BaseModel):
    rank: Rank
    belts: List[Belt]


class Category(BaseModel):
    id: Optional[int] = Field(None, description='ignored when creating (backend generates it)')
    german_name: str
    japan_name: str


class Group(BaseModel):
    id: Optional[int] = Field(None, description='ignored when creating (backend generates it)')
    german_name: str
    japan_name: str
    subcategory_id: int


class Subcategory(BaseModel):
    id: Optional[int] = Field(None, description='ignored when creating (backend generates it)')
    german_name: str
    japan_name: str
    category_id: int


class Image(BaseModel):
    technique_id: int
    name: str


class TechniqueAlias(BaseModel):
    technique_id: int
    alias: str


class Technique(BaseModel):
    id: Optional[int] = Field(None, description='ignored when creating (backend generates it)')
    japan_name: str
    group_id: int
    description: Optional[str] = None
    description_left: Optional[str] = None
    description_right: Optional[str] = None
    rank_position: Optional[int] = None
    variant_of: Optional[int] = None


class MemoryName(BaseModel):
    id: int
    name: str


class MemoryNameType(Enum):
    TECHNIQUE = 'technique'
    GROUP = 'group'


class MemoryPieces(BaseModel):
    names: List[MemoryName]
    image_ids: List[str]
    name_type: MemoryNameType


class MemorySubmission(BaseModel):
    name_id: int
    name_type: MemoryNameType
    image_id: str = Field(..., description='encrypted image name')


class DecryptedMemorySubmission(BaseModel):
    name_id: int
    name_type: MemoryNameType
    image_name: str = Field(..., description='decrypted image name')


class MemorySubmissionResult(BaseModel):
    is_correct: bool
    selected_name: MemoryName
    name_of_selected_image: MemoryName
    selected_image: str
    image_of_selected_name: Optional[str] = None


class TechniqueGameStatistic(BaseModel):
    technique_id: int
    japan_name: str
    correct_answers: int = Field(..., ge=0)
    wrong_answers: int = Field(..., ge=0)


class GroupGameStatistic(BaseModel):
    group_id: int
    japan_name: str
    correct_answers: int = Field(..., ge=0)
    wrong_answers: int = Field(..., ge=0)
