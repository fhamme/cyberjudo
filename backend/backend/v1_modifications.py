from fastapi import APIRouter, Depends

from .auth import is_admin
from .cyberjudo import CyberJudo
from .models import Rank, Belt, Category, Subcategory, Group, Technique


def get_modifications_router(cyberjudo: CyberJudo) -> APIRouter:
    router = APIRouter(dependencies=[Depends(is_admin(cyberjudo))])

    @router.post('/ranks', status_code=201)
    def add_rank(rank: Rank):
        cyberjudo.add_rank(rank)

    @router.post('/belts', status_code=201)
    def add_belt(belt: Belt):
        cyberjudo.add_belt(belt)

    @router.post('/categories', status_code=201)
    def add_category(category: Category):
        cyberjudo.add_category(category)

    @router.post('/subcategories', status_code=201)
    def add_subcategory(subcategory: Subcategory):
        cyberjudo.add_subcategory(subcategory)

    @router.post('/groups', status_code=201)
    def add_group(group: Group):
        cyberjudo.add_group(group)

    @router.post('/techniques', status_code=201)
    def add_technique(technique: Technique):
        cyberjudo.add_technique(technique)

    return router
