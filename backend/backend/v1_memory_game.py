from typing import List

from fastapi import APIRouter, HTTPException

from .crypto import CyberJudoCrypto
from .cyberjudo import CyberJudo, MemoryPieces, MemorySubmission, DecryptedMemorySubmission, MemorySubmissionResult
from .models import MemoryNameType


def get_memory_game_router(cyberjudo: CyberJudo, cjc: CyberJudoCrypto) -> APIRouter:
    router = APIRouter()

    @router.get('/pieces', response_model=MemoryPieces)
    def get_pieces(max_rank: int, max_count: int, category_id: int = None,
                   name_type: MemoryNameType = MemoryNameType.TECHNIQUE):
        cyberjudo.count_game_start(name_type)
        return cyberjudo.memory_pieces(max_rank, max_count, cjc, name_type, category_id)

    @router.post('/submissions', response_model=List[MemorySubmissionResult])
    def validate_matched_memory_pieces(submissions: List[MemorySubmission]):
        decrypted_submissions = list()
        for submission in submissions:
            try:
                image_name = cjc.decrypt(submission.image_id)
            except Exception:
                raise HTTPException(status_code=400, detail=f'unknown image_id in this submission: {submission}')
            decrypted_submissions.append(
                DecryptedMemorySubmission(name_id=submission.name_id, image_name=image_name,
                                          name_type=submission.name_type))
        return cyberjudo.validate_submissions(decrypted_submissions)

    return router
