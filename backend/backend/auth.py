from fastapi import Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from .cyberjudo import CyberJudo


def is_admin(cyberjudo: CyberJudo):
    def _is_admin(credentials: HTTPBasicCredentials = Depends(HTTPBasic())):
        if not cyberjudo.validate_credentials(username=credentials.username, password=credentials.password):
            raise HTTPException(status_code=401, detail='invalid credentials')

    return _is_admin
