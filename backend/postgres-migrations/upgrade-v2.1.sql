begin;

create sequence started_group_games minvalue 0 start with 0;
create sequence started_technique_games minvalue 0 start with 0;
create sequence submitted_group_games minvalue 0 start with 0;
create sequence submitted_technique_games minvalue 0 start with 0;
select setval('started_group_games', 0),
       setval('started_technique_games', 0),
       setval('submitted_group_games', 0),
       setval('submitted_technique_games', 0);

create view counters as
select 'started_technique_games' as counter_name, last_value from started_technique_games
union
select 'started_group_games' as counter_name, last_value from started_group_games
union
select 'submitted_group_games' as counter_name, last_value from submitted_group_games
union
select 'submitted_technique_games' as counter_name, last_value from submitted_technique_games
order by counter_name;

create table technique_game_statistics
(
    technique_id    smallint primary key,
    correct_answers int default 0,
    wrong_answers   int default 0,
    foreign key (technique_id) references techniques (id) on update cascade on delete cascade
);

create table group_game_statistics
(
    group_id        smallint primary key,
    correct_answers int default 0,
    wrong_answers   int default 0,
    foreign key (group_id) references groups (id) on update cascade on delete cascade
);

end;