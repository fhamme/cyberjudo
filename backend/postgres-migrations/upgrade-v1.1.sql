begin;

create extension if not exists pgcrypto;

create table admins
(
    name text primary key,
    password_hash text not null,
    changed_at timestamptz not null default now()
);

commit;