begin;

alter table images add rank_position smallint;
alter table images add foreign key (rank_position) references ranks (position);
comment on column images.rank_position is 'from this rank on, a judoka should detect which technique the image shows';

commit;