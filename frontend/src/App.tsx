import React from 'react';
import {CjFooter} from "./components/footer";
import {GameSelection, GameSelector} from "./pages";
import {
    ApiRankWithBelts,
    Category,
    Config,
    getCategories,
    getConfig,
    getPieces,
    getRanks,
    MemoryGame,
    MemoryImageModel,
    submitMemoryGame
} from "./api/backend";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {Belt} from "./components/belt";
import {ActiveMemoryGame} from "./pages/running-game";
import {FinishedMemoryGame} from "./pages/finished-game";


enum CyberjudoState {
    GAME_OPTIONS,
    GAME_RUNNING,
    GAME_RESULTS
}

interface State {
    gameState: CyberjudoState,
    ranks: ApiRankWithBelts[],
    categories: Category[],
    selectedGame: GameSelection | null,
    memoryGame: MemoryGame | null,
    revealedImages: MemoryImageModel[] | null
}

class App extends React.Component<any, State> {
    private config: Config | undefined;

    constructor(props: any) {
        super(props);
        this.state = {
            gameState: CyberjudoState.GAME_OPTIONS,
            ranks: [],
            categories: [],
            selectedGame: null,
            memoryGame: null,
            revealedImages: null
        };
        getConfig(config => {
            this.config = config;
            getRanks(this.config.backendUrl, (apiRanksWithBelts => {
                this.setState({ranks: apiRanksWithBelts});
            }), error => {
                console.log(error);
            });
            getCategories(this.config.backendUrl, (categories => {
                this.setState({categories: categories});
            }), error => {
                console.log(error);
            });
        }, error => {
            console.log(error);
        });
        this.selectGame = this.selectGame.bind(this);
        this.finishGame = this.finishGame.bind(this);
    }

    render() {
        let content: any;
        switch (this.state.gameState) {
            case CyberjudoState.GAME_OPTIONS:
                content = <GameSelector ranks={this.state.ranks} onSelect={this.selectGame}
                                        categories={this.state.categories}/>;
                break;
            case CyberjudoState.GAME_RUNNING:
                content = <ActiveMemoryGame images={this.state.memoryGame?.images!}
                                            selectedGame={this.state.memoryGame?.selectedGame!}
                                            names={this.state.memoryGame?.names!} finishGame={this.finishGame}/>;
                break;
            case CyberjudoState.GAME_RESULTS:
                content = <FinishedMemoryGame images={this.state.revealedImages!}
                                              selectedGame={this.state.memoryGame?.selectedGame!}
                                              restart={() => this.selectGame(this.state.selectedGame!)}/>;
                break;
        }
        return <div className="App">
            <Belt className='belt-color-black'>
                <h1>
                    <a href='./' className='logo-link'>Cyberjudo</a>
                </h1>
            </Belt>
            <main>
                {content}
            </main>
            <CjFooter/>
        </div>;
    }

    selectGame(selectedGame: GameSelection) {
        this.setState({selectedGame: selectedGame});
        getPieces(selectedGame, this.config?.backendUrl!, memoryGame => {
            this.setState({
                gameState: CyberjudoState.GAME_RUNNING,
                memoryGame: memoryGame,
                selectedGame: selectedGame
            });
        }, error => {
            console.log(error);
        });
    }

    finishGame(answeredImages: MemoryImageModel[]) {
        submitMemoryGame(answeredImages, this.state.selectedGame?.nameType!, this.config?.backendUrl!, resolvedImages => {
            this.setState({
                gameState: CyberjudoState.GAME_RESULTS,
                revealedImages: resolvedImages
            })
        }, error => {
            console.log(error);
        });
    }
}

export default App;
