import fetch from 'node-fetch';
import {GameSelection, NameType} from "../pages";

export enum MemoryImageState {
    UNDECIDED_UNSELECTED,
    DECIDED_UNSELECTED,
    UNDECIDED_SELECTED,
    DECIDED_SELECTED,
    CORRECT,
    WRONG
}

export enum MemoryNameState {
    SELECTABLE,
    ALREADY_USED,
    SELECTED
}

export interface MemoryNameModel {
    id: number;
    name: string;
    state: MemoryNameState;
}

export interface ApiMemoryName {
    id: number;
    name: string;
}

export interface MemoryImageModel {
    index: number;
    id: string;
    src: string;
    state: MemoryImageState;
    decision: ApiMemoryName | null;
    correct: ApiMemoryName | null;
}

export interface MemoryGame {
    images: MemoryImageModel[],
    names: MemoryNameModel[],
    selectedGame: GameSelection
}

export interface ApiMemoryPieces {
    names: ApiMemoryName[],
    image_ids: string[],
    name_type: NameType
}

export interface DetailedTechnique {
    id: number,
    japan_name: string,
    group_id: number,
    description: string,
    description_left: string,
    description_right: string,
    rank_position: number,
    variant_of: number
}

export interface ApiSubmissionResult {
    is_correct: boolean,
    selected_name: ApiMemoryName,
    name_of_selected_image: ApiMemoryName,
    selected_image: string,
    image_of_selected_name: string
}

export interface ApiRank {
    japan_name: string,
    position: number,
    default_belt_color: string
}

export interface ApiBelt {
    position: number,
    color: string,
    german_color_name: string
}

export interface ApiRankWithBelts {
    rank: ApiRank,
    belts: ApiBelt[]
}

export function getRankName(apiRankWithBelts: ApiRankWithBelts) {
    const position_name = apiRankWithBelts.rank.position < 0 ? (-1 * apiRankWithBelts.rank.position) + '. Kyu' : apiRankWithBelts.rank.position + '. Dan';
    const belt_colors = apiRankWithBelts.belts.map(belt => belt.german_color_name).join(' / ');
    return position_name + ' -- ' + apiRankWithBelts.rank.japan_name + ' (' + belt_colors + ')';
}

export interface Category {
    id: number,
    german_name: string,
    japan_name: string
}

function get<T>(url: string): Promise<T> {
    return fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.json()
        })
}

function post<T>(url: string, body: any): Promise<T> {
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {'Content-Type': 'application/json'}
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.json()
        })
}

export interface Config {
    backendUrl: string
}

export function getConfig(onSuccess: (config: Config) => void, onError: (error: any) => void): void {
    get<Config>('/config.json').then(onSuccess).catch(onError);
}

export function getRanks(backendUrl: string, onSuccess: (apiRanksWithBelts: ApiRankWithBelts[]) => void, onError: (error: any) => void): void {
    get<ApiRankWithBelts[]>(backendUrl + '/ranks?with_belts=true').then(onSuccess).catch(onError);
}

export function getCategories(backendUrl: string, onSuccess: (categories: Category[]) => void, onError: (error: any) => void): void {
    get<Category[]>(backendUrl + '/categories').then(onSuccess).catch(onError);
}

export function getPieces(selectedGame: GameSelection, backendUrl: string, onSuccess: (memoryGame: MemoryGame) => void, onError: (error: any) => void): void {
    let url = backendUrl + '/memory/pieces?max_count=4&max_rank=' + selectedGame.rank.rank.position;
    if (selectedGame.category != null) {
        url += '&category_id=' + selectedGame.category.id;
    }
    if (selectedGame.nameType === NameType.TECHNIQUE) {
        url += '&name_type=technique';
    } else if (selectedGame.nameType === NameType.GROUP) {
        url += '&name_type=group';
    }
    get<ApiMemoryPieces>(url).then((apiMemoryPieces: ApiMemoryPieces) => {
        onSuccess({
                images: apiMemoryPieces.image_ids.map((image_id: string, index: number) => {
                    return {
                        index: index + 1,
                        id: image_id,
                        src: backendUrl + '/images/anonymous-images/' + image_id,
                        state: MemoryImageState.UNDECIDED_UNSELECTED,
                        decision: null,
                        correct: null
                    };
                }),
                names: apiMemoryPieces.names.map((name: ApiMemoryName) => {
                    return {id: name.id, name: name.name, state: MemoryNameState.SELECTABLE};
                }),
                selectedGame: selectedGame
            }
        )
    }).catch(onError);
}

export function submitMemoryGame(images: MemoryImageModel[], name_type: NameType, backendUrl: string, onSuccess: (resolvedImages: MemoryImageModel[]) => void, onError: (error: any) => void) {
    post<ApiSubmissionResult[]>(backendUrl + '/memory/submissions', images.map(image => {
        if (image.decision === null) {
            throw new Error('decision must be set to submit the image with id' + image.id);
        }
        let api_name_type;
        if (name_type === NameType.TECHNIQUE) {
            api_name_type = 'technique';
        } else if (name_type === NameType.GROUP) {
            api_name_type = 'group';
        }
        return {name_id: image.decision.id, image_id: image.id, name_type: api_name_type};
    })).then(response => {
        onSuccess(response.map((result: ApiSubmissionResult, index: number) => {
            return {
                id: result.selected_image,
                index: index + 1,
                src: backendUrl + '/images/named-images/' + result.selected_image,
                state: result.is_correct ? MemoryImageState.CORRECT : MemoryImageState.WRONG,
                decision: result.selected_name,
                correct: result.name_of_selected_image,
            };
        }));
    }).catch(onError);
}
