export function Belt(props: { className: string, children: any }) {
    return <div className={'w-100 py-1 px-3 belt ' + props.className}>
        {props.children}
    </div>
}