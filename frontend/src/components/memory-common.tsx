import "./quiz.css";
import {Container} from "react-bootstrap";
import {
    ApiMemoryName,
    getRankName,
    MemoryImageModel,
    MemoryImageState,
    MemoryNameModel,
    MemoryNameState
} from "../api/backend";
import {GameSelection, NameType} from "../pages";
import {Belt} from "./belt";


export function MemoryImages(props: { images: MemoryImageModel[], onSelect: (imageIndex: number) => void }) {
    return <div className='m-4 text-center'>
        {props.images.sort((a: MemoryImageModel, b: MemoryImageModel) => a.index - b.index).map(
            image => <MemoryImage key={image.index} {...image} onSelect={() => props.onSelect(image.index)}/>
        )}
    </div>;
}

function MemoryImage(props: {
    index: number,
    id: string,
    src: string,
    state: MemoryImageState,
    decision: ApiMemoryName | null,
    correct: ApiMemoryName | null, onSelect: () => void
}) {
    let descriptionStyle;
    let description;
    let boxStyle = 'border border-secondary d-inline-block m-4 position-relative';
    switch (props.state) {
        case MemoryImageState.UNDECIDED_UNSELECTED:
            descriptionStyle = 'bg-light font-italic';
            description = 'Noch nicht entschieden.';
            break;
        case MemoryImageState.UNDECIDED_SELECTED:
            descriptionStyle = 'bg-light font-italic';
            description = 'Entscheide jetzt!';
            boxStyle += ' thickBorder';
            break;
        case MemoryImageState.CORRECT:
            descriptionStyle = 'bg-success text-white';
            description = props.decision?.name + ', korrekt!';
            break;
        case MemoryImageState.WRONG:
            descriptionStyle = 'bg-danger text-white';
            description = 'Nicht ' + props.decision?.name + ', sondern ' + props.correct?.name + '!';
            break;
        case MemoryImageState.DECIDED_SELECTED:
            boxStyle += ' thickBorder';
            descriptionStyle = 'bg-warning';
            description = props.decision?.name + '?';
            break;
        case MemoryImageState.DECIDED_UNSELECTED:
            descriptionStyle = 'bg-warning';
            description = props.decision?.name + '?';
    }
    return <div className={boxStyle} onClick={props.onSelect}>
        <img src={props.src} alt={'technik-' + props.index} className='memoryImageWidth'/>
        <span className='memoryNumber'>{props.index}</span>
        <div className={descriptionStyle + ' pr-5 pl-2 memoryImageWidth'}>{description}</div>
    </div>;
}

export function MemoryNames(props: {
    names: MemoryNameModel[], onSelect: (selectedName: MemoryNameModel) => void
}) {
    return <Container>
        {props.names.map((name, index) => <MemoryName name={name} key={index} onSelect={props.onSelect}/>)}
    </Container>;
}

function MemoryName(props: { name: MemoryNameModel, onSelect: (name: MemoryNameModel) => void }) {
    let nameStyle = 'p-2 m-3 border-secondary border d-inline-block memoryName';
    let name: string | JSX.Element = props.name.name;
    switch (props.name.state) {
        case MemoryNameState.ALREADY_USED:
            nameStyle += ' bg-light';
            name = <del>{name}</del>;
            break;
        case MemoryNameState.SELECTABLE:
            nameStyle += ' bg-light';
            break;
        case MemoryNameState.SELECTED:
            nameStyle += ' font-bold bg-warning thickBorder';
    }
    return <div className={nameStyle} onClick={() => props.onSelect(props.name)}>
        {name}
    </div>;
}

export function CheckButton(props: { wasClicked: boolean, enabled: boolean, onClick: () => void }) {
    let buttonStyle = 'border border-secondary p-2 text-center submitButton';
    buttonStyle += props.enabled ? ' bg-success text-white' : ' bg-light text-secondary submitButtonDisabled';
    let text = 'Antworten überprüfen';
    if (props.wasClicked) {
        text = 'Antworten werden überprüft...';
        buttonStyle += ' submitButtonClicked';
    }
    return <div className={buttonStyle} onClick={props.onClick}>
        {text}
    </div>;
}

export function GameSelectionInfo(props: { selectedGame: GameSelection }) {
    let category: string;
    if (props.selectedGame.category == null) {
        category = 'Techniken';
    } else {
        category = props.selectedGame.category.japan_name + ' (' + props.selectedGame.category.german_name + ') ';
    }
    let quizType: string;
    if (props.selectedGame.nameType === NameType.TECHNIQUE) {
        quizType = 'Techniken-Quiz';
    } else {
        quizType = 'Technik-Gruppen-Quiz';
    }
    return <Belt className={'belt-color-' + (props.selectedGame.rank.rank.default_belt_color || 'unknown')}>
        {quizType} aus {category} bis zum {getRankName(props.selectedGame.rank)}
    </Belt>;
}