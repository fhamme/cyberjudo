import {Belt} from "./belt";

export function CjFooter() {
    return <Belt className='footer belt-color-black'>
        Cyberjudo -- Copyright 2021 Felix Hamme -- <a href='https://gitlab.com/fhamme/cyberjudo' target='_blank' rel='noreferrer'>sourcecode</a>
    </Belt>
}