import {MemoryImageModel} from "../api/backend";
import {Button, Container} from "react-bootstrap";
import {GameSelectionInfo, MemoryImages} from "../components/memory-common";
import {GameSelection} from "./index";

export function FinishedMemoryGame(props: { images: MemoryImageModel[], restart: () => void, selectedGame: GameSelection }) {
    return <div>
        <GameSelectionInfo selectedGame={props.selectedGame}/>
        <Container className='mt-3 text-center'>
            <h2>Spielergebnisse</h2>
        </Container>
        <MemoryImages images={props.images} onSelect={() => {
        }}/>
        <Container className='text-center pt-4'>
            <Button variant='success' className='mx-2 rounded-0' onClick={props.restart}>neues Spiel</Button>
            <Button variant='success' className='mx-2 rounded-0' href={window.location.href}>neues Spiel mit anderer
                Auswahl</Button>
        </Container>
    </div>;
}