import {Belt} from "../components/belt";
import {ApiRankWithBelts, Category, getRankName} from "../api/backend";
import {Component} from "react";
import {Container} from "react-bootstrap";

export enum NameType {
    TECHNIQUE,
    GROUP
}

export interface GameSelection {
    rank: ApiRankWithBelts,
    category: Category | null,
    nameType: NameType
}

export interface GameSelectorState {
    selectedRankPosition: number | null,
    selectedCategoryId: number | null,
    selectedNameType: NameType
}

interface GameSelectorProps {
    ranks: ApiRankWithBelts[],
    onSelect: (selectedRank: GameSelection) => void,
    categories: Category[]
}

export class GameSelector extends Component<GameSelectorProps, GameSelectorState> {

    constructor(props: GameSelectorProps) {
        super(props);
        this.state = {
            selectedRankPosition: null,
            selectedCategoryId: null,
            selectedNameType: NameType.TECHNIQUE
        }
        this.onSelect = this.onSelect.bind(this);
    }

    onSelect() {
        this.props.onSelect({
            rank: this.props.ranks.find(rank => rank.rank.position === this.state.selectedRankPosition)!,
            category: this.props.categories.find(category => category.id === this.state.selectedCategoryId)!,
            nameType: this.state.selectedNameType
        });
    }

    render() {
        return <Container className='my-5'>
            <div className='row mb-5'>
                <div className='col-md'>
                    <h2>Für welchen Gürtel möchtest Du üben?</h2>
                    <RankSelector ranks={this.props.ranks} selectedRankPosition={this.state.selectedRankPosition}
                                  onSelect={selectedRankPosition => this.setState({selectedRankPosition: selectedRankPosition})}/>
                </div>
                <div className='col-md'>
                    <h2>Für welche Kategorie möchtest Du üben?</h2>
                    <CategorySelector categories={this.props.categories}
                                      selectedCategoryId={this.state.selectedCategoryId}
                                      onSelect={selectedCategoryId => this.setState({selectedCategoryId: selectedCategoryId})}/>
                </div>
                <div className='col-md'>
                    <h2>Welche Namen sollen abgefragt werden?</h2>
                    <NameTypeSelector selectedNameType={this.state.selectedNameType}
                                      onSelect={selectedNameType => this.setState({selectedNameType: selectedNameType})}/>
                </div>
            </div>
            <StartGameButton isEnabled={this.state.selectedRankPosition !== null} onClick={this.onSelect}/>
        </Container>;
    }
}

function StartGameButton(props: { isEnabled: boolean, onClick: () => void }) {
    if (props.isEnabled) {
        return <a onClick={props.onClick} className='submitButton text-center'>
            <Belt className='belt-color-black'>Spiel starten</Belt>
        </a>
    } else {
        return <a className='submitButtonDisabled text-center'>
            <Belt className='belt-color-black inactive'>Spiel starten</Belt>
        </a>
    }
}

function RankSelector(props: { ranks: ApiRankWithBelts[], selectedRankPosition: number | null, onSelect: (selectedRankPosition: number) => void }) {
    return <table className='w-100'>
        {props.ranks.map(rankWithBelts => {
            return <Rank rankWithBelts={rankWithBelts} key={rankWithBelts.rank.position} onSelect={props.onSelect}
                         isSelected={props.selectedRankPosition === null || props.selectedRankPosition === rankWithBelts.rank.position}/>;
        })}
    </table>;
}

function Rank(props: { rankWithBelts: ApiRankWithBelts, isSelected: boolean, onSelect: (selectedRankPosition: number) => void }) {
    const rankDescription = getRankName(props.rankWithBelts);
    let beltColor = 'belt-color-' + (props.rankWithBelts.rank.default_belt_color || 'unknown');
    if (!props.isSelected) {
        beltColor += ' inactive';
    }
    return <tr>
        <td>
            <a onClick={_ => props.onSelect(props.rankWithBelts.rank.position)}
               className={beltColor + ' belt-link my-3'}>
                <Belt className='my-3'>
                    {rankDescription}
                </Belt>
            </a>
        </td>
    </tr>;
}

function SelectableItem(props: { name: string, isSelected: boolean, onSelect: () => void, beltColor: string }) {
    let beltStyle = 'my-3 selectable-item ' + props.beltColor;
    if (!props.isSelected) {
        beltStyle += ' inactive'
    }
    return <tr>
        <td><a onClick={props.onSelect}>
            <Belt className={beltStyle}>{props.name}</Belt>
        </a></td>
    </tr>;
}

function CategorySelector(props: { categories: Category[], selectedCategoryId: number | null, onSelect: (selectedCategoryId: number | null) => void }) {
    return <table className='w-100'>
        <SelectableItem name='alle Kategorien' isSelected={props.selectedCategoryId === null}
                        beltColor='belt-color-blue'
                        onSelect={() => props.onSelect(null)}/>
        {props.categories.map(category => <SelectableItem key={category.id} beltColor='belt-color-blue'
                                                          name={category.japan_name + ' (' + category.german_name + ')'}
                                                          isSelected={props.selectedCategoryId === category.id}
                                                          onSelect={() => props.onSelect(category.id)}/>
        )}
    </table>
}

function NameTypeSelector(props: { selectedNameType: NameType, onSelect: (nameType: NameType) => void }) {
    return <table className={'w-100'}>
        <SelectableItem name='Techniken' isSelected={props.selectedNameType === NameType.TECHNIQUE}
                        onSelect={() => props.onSelect(NameType.TECHNIQUE)} beltColor='belt-color-orange'/>
        <SelectableItem name='Technik-Gruppen' isSelected={props.selectedNameType === NameType.GROUP}
                        onSelect={() => props.onSelect(NameType.GROUP)} beltColor='belt-color-orange'/>
    </table>
}