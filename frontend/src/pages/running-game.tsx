import {Component} from "react";
import {MemoryImageModel, MemoryImageState, MemoryNameModel, MemoryNameState} from "../api/backend";
import {Container} from "react-bootstrap";
import {CheckButton, GameSelectionInfo, MemoryImages, MemoryNames} from "../components/memory-common";
import {GameSelection, NameType} from "./index";

interface ActiveMemoryGameState {
    images: MemoryImageModel[],
    names: MemoryNameModel[],
    checkButtonEnabled: boolean,
    resultRequested: boolean
}

interface ActiveMemoryGameProps {
    images: MemoryImageModel[],
    names: MemoryNameModel[],
    finishGame: (answeredImages: MemoryImageModel[]) => void,
    selectedGame: GameSelection
}

export class ActiveMemoryGame extends Component<ActiveMemoryGameProps, ActiveMemoryGameState> {
    private readonly finishGame: (answeredImages: MemoryImageModel[]) => void;
    private readonly selectedGame: GameSelection;

    constructor(props: ActiveMemoryGameProps) {
        super(props);
        this.state = {
            images: props.images,
            names: props.names,
            checkButtonEnabled: false,
            resultRequested: false
        };
        this.selectName = this.selectName.bind(this);
        this.selectImage = this.selectImage.bind(this);
        this.onCheckButtonClick = this.onCheckButtonClick.bind(this);
        this.finishGame = props.finishGame;
        this.selectedGame = props.selectedGame;
    }

    render() {
        const nameTypeSingular = this.selectedGame.nameType === NameType.TECHNIQUE ? 'Technik' : 'Gruppe';
        return <div>
            <GameSelectionInfo selectedGame={this.selectedGame}/>
            <Container className='mt-3 text-center'>
                <h2>Ordne jedem Bild die richtige {nameTypeSingular} zu!</h2>
                <p>Wähle erst ein Bild aus, dann einen Namen, um sie zuzuordnen.</p>
            </Container>
            <MemoryImages images={this.state.images} onSelect={this.selectImage}/>
            <MemoryNames names={this.state.names} onSelect={this.selectName}/>
            <Container className='my-4'>
                <CheckButton enabled={this.state.checkButtonEnabled} wasClicked={this.state.resultRequested}
                             onClick={this.onCheckButtonClick}/>
            </Container>
        </div>;
    }

    selectName(selectedName: MemoryNameModel) {
        if (this.state.resultRequested) {
            // don't change anything after the selections are submitted
            return;
        }
        let newNames = [];
        let newImages = [];
        switch (selectedName.state) {
            case MemoryNameState.ALREADY_USED:
                return;

            case MemoryNameState.SELECTED:
                // deselect name
                for (const oldName of this.state.names) {
                    let newNameState = oldName.state === MemoryNameState.SELECTED ? MemoryNameState.SELECTABLE : oldName.state;
                    newNames.push({id: oldName.id, name: oldName.name, state: newNameState});
                }
                // remove decision from image
                for (const oldImage of this.state.images) {
                    if (oldImage.state === MemoryImageState.DECIDED_SELECTED) {
                        newImages.push({
                            index: oldImage.index,
                            src: oldImage.src,
                            state: MemoryImageState.UNDECIDED_SELECTED,
                            decision: null,
                            id: oldImage.id,
                            correct: oldImage.correct
                        });
                    } else {
                        newImages.push(oldImage);
                    }
                }
                break;

            case MemoryNameState.SELECTABLE:
                // select name
                for (const oldName of this.state.names) {
                    let newNameState = oldName.state;
                    if (oldName.id === selectedName.id) {
                        newNameState = MemoryNameState.SELECTED;
                    } else if (oldName.state === MemoryNameState.SELECTED) {
                        newNameState = MemoryNameState.SELECTABLE;
                    }
                    newNames.push({id: oldName.id, name: oldName.name, state: newNameState});
                }
                // update image decision
                for (const oldImage of this.state.images) {
                    if (oldImage.state === MemoryImageState.DECIDED_SELECTED || oldImage.state === MemoryImageState.UNDECIDED_SELECTED) {
                        newImages.push({
                            index: oldImage.index,
                            src: oldImage.src,
                            state: MemoryImageState.DECIDED_SELECTED,
                            decision: {id: selectedName.id, name: selectedName.name},
                            id: oldImage.id,
                            correct: oldImage.correct
                        });
                    } else {
                        newImages.push(oldImage);
                    }
                }
                break;
        }
        this.setState({
            images: newImages,
            names: newNames,
            checkButtonEnabled: this.shouldCheckButtonBeEnabled(newImages)
        });
    }

    selectImage(selectedImageIndex: number) {
        if (this.state.resultRequested) {
            // don't change anything after the selections are submitted
            return;
        }
        if (this.state.images.every(image => image.state === MemoryImageState.UNDECIDED_UNSELECTED)) {
            const selectedName = this.state.names.find(name => name.state === MemoryNameState.SELECTED);
            if (selectedName !== undefined) {
                // In case a name is selected before any image, update the image decision to the selected name when
                // an image is selected.

                const newImages = [];
                for (const oldImage of this.state.images) {
                    if (oldImage.index === selectedImageIndex) {
                        newImages.push({
                            index: oldImage.index,
                            src: oldImage.src,
                            state: MemoryImageState.DECIDED_SELECTED,
                            decision: {id: selectedName.id, name: selectedName.name},
                            id: oldImage.id,
                            correct: oldImage.correct
                        });
                    } else {
                        newImages.push(oldImage);
                    }
                }
                // apply changes
                this.setState({images: newImages});
                return;
            }
        }
        const oldImage = this.state.images.find(image => image.index === selectedImageIndex);
        switch (oldImage?.state) {
            case MemoryImageState.DECIDED_SELECTED:
            case MemoryImageState.UNDECIDED_SELECTED:
            case MemoryImageState.CORRECT:
            case MemoryImageState.WRONG:
                return;
            case MemoryImageState.UNDECIDED_UNSELECTED:
            case MemoryImageState.DECIDED_UNSELECTED:

                // update images
                const newImages = this.state.images.map(image => {
                    return {
                        index: image.index,
                        id: image.id,
                        src: image.src,
                        state: this.updateImageState(image, selectedImageIndex),
                        decision: image.decision,
                        correct: image.correct,
                    }
                });

                // update names
                const newNames = [];
                for (let oldName of this.state.names) {
                    let newNameState = this.updateNameState(oldName, oldImage);
                    newNames.push({id: oldName.id, state: newNameState, name: oldName.name});
                }

                // apply changes
                this.setState({images: newImages, names: newNames});
        }
    }

    updateImageState(oldImage: MemoryImageModel, selectedImageIndex: number) {
        let newImageState = oldImage.state;
        if (oldImage.index === selectedImageIndex) {
            if (oldImage.state === MemoryImageState.DECIDED_UNSELECTED) {
                newImageState = MemoryImageState.DECIDED_SELECTED;
            } else {
                newImageState = MemoryImageState.UNDECIDED_SELECTED;
            }
        } else if (oldImage.state === MemoryImageState.UNDECIDED_SELECTED) {
            newImageState = MemoryImageState.UNDECIDED_UNSELECTED;
        } else if (oldImage.state === MemoryImageState.DECIDED_SELECTED) {
            newImageState = MemoryImageState.DECIDED_UNSELECTED;
        }
        return newImageState;
    }

    updateNameState(oldName: MemoryNameModel, oldImage: MemoryImageModel) {
        let newState = oldName.state;
        if (oldName.state === MemoryNameState.SELECTED) {
            newState = MemoryNameState.ALREADY_USED;
        } else if (oldName.state === MemoryNameState.ALREADY_USED) {
            if (oldImage.state === MemoryImageState.DECIDED_UNSELECTED) {
                if (oldImage.decision?.id === oldName.id) {
                    newState = MemoryNameState.SELECTED;
                }
            }
        }
        return newState;
    }

    shouldCheckButtonBeEnabled(images: MemoryImageModel[]) {
        if (this.state.resultRequested) {
            return false;
        }
        const imageStates = images.map(image => image.state);
        const includesUndecided = imageStates.includes(MemoryImageState.UNDECIDED_SELECTED) || imageStates.includes(MemoryImageState.UNDECIDED_UNSELECTED);
        return !includesUndecided;
    }

    onCheckButtonClick() {
        if (!this.state.checkButtonEnabled) {
            return;
        }
        this.setState({resultRequested: true});
        this.finishGame(this.state.images);
    }
}