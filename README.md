# Cyberjudo - Online Judo lernen

## Backend
The backend reads at startup from a data directory and then serves the data via a REST API.
The data directory contains yaml files describing judo techniques or the like, and plain image files which will be served statically.

## Deployment
See [backend/README.md](backend/README.md) and [frontend/README.md](backend/README.md) for more details about how to start development servers or deploy to production.

Start the [frontend on port 3000](http://localhost:3000) and the [backend on port 5000](http://localhost:5000/v1):
```shell script
# create the docker images (required after every code change)
docker-compose build

# start the servers (will start at system boot too)
docker-compose up -d

# stop (won't start at system boot again)
docker-compose down

# see logs
docker-compose logs
```
The instructions above require [docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/).
